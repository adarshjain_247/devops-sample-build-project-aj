@Grab("com.fasterxml.jackson.core:jackson-annotations:2.9.0")
@Grab('com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.9.0')
@Grab('com.fasterxml.jackson.core:jackson-databind:2.2.3')
@Grab('com.squareup.okhttp3:okhttp:4.2.1')
@Grab('com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.9.0')
@Grab('com.fasterxml.jackson.core:jackson-databind:2.2.3')
@Grab('com.squareup.okhttp3:okhttp:4.2.1')

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import groovy.text.SimpleTemplateEngine
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody

import java.io.File
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

class SandboxActionGroup {
    public String actionGroup;
    public List<String> actions;
    public String roleEntry;
    public List<String> roles;
}

class SandboxObject {
    public String object;
    public String objectGroup;
    public String objectGroupFunction;
    public List<SandboxActionGroup> actionGroups;
}

class RequestDefinition {
    public String method;
    public String url;
    public String body;

    public RequestDefinition(String method, String url, String body) {
        this.method = method;
        this.url = url;
        this.body = body;
    }
}

RequestConfigurations = [
        "Objects"                  : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object', """
            {
                "status": "ACTIVE"
            }
            """),
        "ObjectGroups"             : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/object_groups/$object_group', '''
            {
                "description": "$object_group",
                "rule": {
                    "functionLanguage": "JAVASCRIPT",
                    "function": "$object_group_function"
                },
                "status": "ACTIVE"
            }
            '''),
        "Actions"                  : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/actions/$action', '''
            {
                "description": "$action",
                "status": "ACTIVE"
            }
            '''),
        "ActionGroups"             : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/action_groups/$action_group', '''
            {
                "description": "$action_group",
                "status": "ACTIVE"
            }
            '''),
        "MapActionToActionGroups"  : new RequestDefinition("PATCH", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/action_groups/$action_group/actions/$action', ""),
        "RoleEntries"              : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/role_entries/$role_entry', '''
            {
                "description": "Wrapper for operations on $role_entry",
                "status": "ACTIVE"
            }
            '''),
        "MapActionGroupToRoleEntry": new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/role_entries/$role_entry/action_groups/$action_group', ""),
        "MapObjectGroupToRoleEntry": new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/object_types/$object/role_entries/$role_entry/object_groups/$object_group', ""),
        "Roles"                    : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/roles/$role', '''
            {
                "description": "$role",
                "status": "ACTIVE"
            }
            '''),
        "MapRoleToRoleEntry"       : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/roles/$role/role_entries/$role_entry', ""),
        "Scopes"                   : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/scopes/$scope', '''
            {
                "description": "$scope",
                "status": "ACTIVE"
            }
            '''),
        "MapScopeToRoleEntry"      : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/scopes/$scope/role_entries/$role_entry', ""),
        "Users"                    : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/realms/$realm/subjects/$subject/roles/$role', ""),
        "MapScopeToRealm"          : new RequestDefinition("POST", '$host_url/sandbox/tenants/$tenant/sandboxes/$sandbox/realms/$realm/scopes/$scope', ""),
]

client = client = new OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build();
engine = new SimpleTemplateEngine()

def makeRequest(String configKey, String authToken, Map variables, Boolean dry) {
    def config = RequestConfigurations[configKey]

    def reqUrl = engine.createTemplate(config.url).make(variables).toString()
    def jsonBody = null
    if (config.body) {
        jsonBody = engine.createTemplate(config.body).make(variables).toString()
    }
    if (dry) {
        println "Dry Run"
    }
    println "\u001b[33;5;33mRequest [$configKey]:\u001b[0m method= " + config.method + " url=" + reqUrl + " body=" + (jsonBody ? jsonBody : "{}");
    if (dry) {
        return
    }
    def reqBody = RequestBody.create(MediaType.get("application/json; charset=utf-8"), jsonBody ? jsonBody : "{}")
    def req = new Request.Builder()
            .url(reqUrl)
            .method(config.method, reqBody)
            .header("Authorization", "Bearer " + authToken)
            .build();
    client.newCall(req).execute().withCloseable { response ->
        def statusCode = response.code();
        if (statusCode > 205) {
            String message = response.body().string()
            println "message: " + message
            if (!message.contains("ALREADY_EXISTS")) {
                throw new RuntimeException(statusCode + " " + message)
            }
        }
    }
}

def configure(String fileLoc, String domain, String tenant, String sandbox, String scope, String host_url, Boolean dry, String authToken) {
    def postfix = "_" + tenant+"_"+sandbox;
    ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
    List<SandboxObject> objects = yamlMapper.readValue(new File(fileLoc), new TypeReference<List<SandboxObject>>() {
    })
    def commonVariables = [
            "host_url": host_url,
            "domain"  : domain,
            "realm"   : domain,
            "tenant"  : tenant,
            "sandbox" : sandbox,
            "scope"   : scope,
    ]
    objects.each { so ->
        def variables = commonVariables.clone()
        variables["object"] = so.object
        makeRequest("Objects", authToken, variables, dry)

        if(so.objectGroup) {
            variables["object_group"] = so.objectGroup + postfix;
        }else {
            variables["object_group"] = so.object + "_object_grp" + postfix
        }

        if(so.objectGroupFunction) {
            variables["object_group_function"] = so.objectGroupFunction;
        }else {
            variables["object_group_function"] = "function apply(){return true}";
        }

        makeRequest("ObjectGroups", authToken, variables, dry)

        so.actionGroups.each { actionGroup ->
            actionGroup.actions.each { action ->
                variables["action"] = action
                makeRequest("Actions", authToken, variables, dry)
            }
            variables["action_group"] = actionGroup.actionGroup + postfix
            makeRequest("ActionGroups", authToken, variables, dry)
            actionGroup.actions.each { action ->
                variables["action"] = action
                makeRequest("MapActionToActionGroups", authToken, variables, dry)
            }

            variables["role_entry"] = actionGroup.roleEntry + postfix
            makeRequest("RoleEntries", authToken, variables, dry)
            makeRequest("MapObjectGroupToRoleEntry", authToken, variables, dry)
            makeRequest("MapActionGroupToRoleEntry", authToken, variables, dry)
            // assuming scope exists
            makeRequest("Scopes", authToken, variables, dry)
            makeRequest("MapScopeToRoleEntry", authToken, variables, dry)

            def createdRoles = []
            actionGroup.roles.each { role ->
                if (!createdRoles.contains(role)) {
                    createdRoles.add(role)
                    variables["role"] = role;
                    makeRequest("Roles", authToken, variables, dry)
                }
                variables["role_entry"] = actionGroup.roleEntry + postfix
                makeRequest("MapRoleToRoleEntry", authToken, variables, dry)
            }
        }
        makeRequest("MapScopeToRealm", authToken, variables, dry)

    }
}

def cli = new CliBuilder(usage: 'issuer.groovy [options]')
cli.h(longOpt: "help", type: String, 'Help', required: false)
cli.f(longOpt: "file", type: String, 'The yaml file', required: true)
cli.d(longOpt: "domain", type: String, 'The domain id', required: true)
cli.t(longOpt: "tenant", type: String, 'The tenant id', required: true)
cli.s(longOpt: "sandbox", type: String, 'The sandbox id', required: true)
cli.q(longOpt: "scope", type: String, 'The default scope', required: true)
cli.w(longOpt: "host_url", type: String, 'The sandbox api url', required: true)
cli.p(longOpt: "dry", type: Boolean, 'Dry run will print only api calls', required: false)
cli.a(longOpt: "auth", type: String, 'The auth token', required: true)

def options = cli.parse(this.args)
if (!options) {
    return
}
configure(options.f, options.d, options.t, options.s, options.q, options.w, options.p, options.a)
