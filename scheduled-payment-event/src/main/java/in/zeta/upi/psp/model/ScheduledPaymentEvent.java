package in.zeta.upi.psp.model;


public class ScheduledPaymentEvent {
  private Long ifi;
  private String paymentRequestId;
  private String resourceId;
  private String requestFrom;
  private Long delay;
  private int retryCount;
  private String subType;

  public ScheduledPaymentEvent(Long ifi, String paymentRequestId, String resourceId, String requestFrom, Long delay, int retryCount, String subType) {
    this.ifi = ifi;
    this.paymentRequestId = paymentRequestId;
    this.resourceId = resourceId;
    this.requestFrom = requestFrom;
    this.delay = delay;
    this.retryCount = retryCount;
    this.subType = subType;
  }

  public Long getIfi() {
    return ifi;
  }

  public void setIfi(Long ifi) {
    this.ifi = ifi;
  }

  public String getPaymentRequestId() {
    return paymentRequestId;
  }

  public void setPaymentRequestId(String paymentRequestId) {
    this.paymentRequestId = paymentRequestId;
  }

  public String getResourceId() {
    return resourceId;
  }

  public void setResourceId(String resourceId) {
    this.resourceId = resourceId;
  }

  public String getRequestFrom() {
    return requestFrom;
  }

  public void setRequestFrom(String requestFrom) {
    this.requestFrom = requestFrom;
  }

  public Long getDelay() {
    return delay;
  }

  public void setDelay(Long delay) {
    this.delay = delay;
  }

  public int getRetryCount() {
    return retryCount;
  }

  public void setRetryCount(int retryCount) {
    this.retryCount = retryCount;
  }

  public String getSubType() {
    return subType;
  }

  public void setSubType(String subType) {
    this.subType = subType;
  }
}
