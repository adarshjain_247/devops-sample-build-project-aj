package in.zeta.upi.psp;

import action.IAction;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import in.zeta.upi.psp.model.ScheduledPaymentEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ScheduledPaymentEventHandler implements IAction {

  @Override
  public JsonObject run(JsonObject args) {
    Gson gson = new Gson();
    System.out.println("Received ScheduledPaymentEvent at " + System.currentTimeMillis() + " ms");

    try {
      ScheduledPaymentEvent scheduledPaymentEvent = gson.fromJson(
              args.get("data").getAsJsonObject().get("body"), ScheduledPaymentEvent.class);
      String rawData = gson.toJson(scheduledPaymentEvent);
      String[] baseUrls = args.get("url").getAsString().split(",");
      Arrays.stream(baseUrls).forEach(baseUrl -> {
        try {
          sendRequest(args, scheduledPaymentEvent, rawData, baseUrl);
        } catch (Exception e) {
          System.out.println("Error occurred while processing ScheduledPaymentEvent for url: " + baseUrl
                  + " " + e.getMessage());
        }
      });
    } catch (Exception exception) {
      System.out.println("Error occurred while processing ScheduledPaymentEvent " + exception.getMessage());
    }
    return args;
  }

  private static void sendRequest(JsonObject args, ScheduledPaymentEvent scheduledPaymentEvent, String rawData, String baseUrl) throws IOException {
    String statusCheckUrl = baseUrl + "tenants" + "/" + scheduledPaymentEvent.getIfi() + "/statusCheck";
    String authToken = "Bearer " + args.get("token").getAsString();
    String type = "application/json";
    String encodedData = URLEncoder.encode(rawData, "UTF-8");
    System.out.println("Aster request: " + rawData);
    URL u = new URL(statusCheckUrl);
    HttpURLConnection conn = (HttpURLConnection) u.openConnection();
    conn.setDoOutput(true);
    conn.setRequestMethod("POST");
    conn.setRequestProperty("Content-Type", type);
    conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
    conn.setRequestProperty("Authorization", authToken);
//    System.out.println("Token : "+ authToken);
    try (OutputStream os = conn.getOutputStream()) {
      byte[] input = rawData.getBytes(StandardCharsets.UTF_8);
      os.write(input, 0, input.length);
    }
    String res = new BufferedReader(new InputStreamReader(conn.getInputStream()))
            .lines().parallel().collect(Collectors.joining("\n"));
    System.out.println(res);
    conn.disconnect();
  }
}
