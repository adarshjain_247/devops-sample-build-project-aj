//import in.zeta.upi.psp.AtroposPublisher;
//import in.zeta.upi.psp.model.ScheduledPaymentEvent;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.UUID;
//import java.util.concurrent.CompletableFuture;
//import java.util.concurrent.ExecutionException;
//
//@RunWith(SpringRunner.class)
//public class AtroposPublisherTest {
//  private AtroposPublisher atroposPublisher = new AtroposPublisher();
//  @Test
//  public void publishTest() throws ExecutionException, InterruptedException {
//    CompletableFuture<Void> completableFuture = (CompletableFuture<Void>) atroposPublisher.publish(ScheduledPaymentEvent.builder()
//            .ifi(151613L)
//            .paymentRequestId(UUID.randomUUID().toString())
//            .requestFrom("vpa://8283393900@hdfczeta")
//            .retryCount(1)
//            .build());
//    completableFuture.get();
//  }
//}
