package in.zeta.upi.util;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

public class DateUtils {

    private DateUtils() {}

    public static String currentTimeStampWithNPCIDefaultOffset() {
        OffsetDateTime now = Instant.now().atZone(ZoneId.systemDefault()).toOffsetDateTime();
        return now.format(ISO_OFFSET_DATE_TIME);
    }

}