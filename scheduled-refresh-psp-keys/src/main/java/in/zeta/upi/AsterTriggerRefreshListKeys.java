package in.zeta.upi;

import action.IAction;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonToken;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import in.zeta.commons.settings.SettingsModule;
import in.zeta.crux.services.TokenService;
import in.zeta.upi.util.DateUtils;
import in.zeta.upi.util.IdGenerator;
import olympus.common.JID;
import olympus.message.processor.Interceptor;
import olympus.message.processor.MessagingService;
import olympus.message.processor.OMSClient;
import olympus.message.processor.serial.SerialProcessor;
import olympus.message.types.Message;
import olympus.message.types.OlympusMessage;
import olympus.pubsub.CompositeNodeDrainHandler;
import olympus.pubsub.PubSubMessagingService;
import olympus.pubsub.model.PubSubEvent;
import olympus.pubsub.subscription.PubSubMessageCallBack;
import olympus.pubsub.subscription.Subscription;
import olympus.spartan.ImmutableMessage;
import olympus.spartan.NodeDrainHandler;
import olympus.spartan.RegistrationHandler;
import olympus.spartan.RemoteServiceInstance;
import olympus.spartan.RouterMessage;
import olympus.spartan.Spartan;
import olympus.spartan.lookup.BucketDrainHandler;
import olympus.spartan.util.ServiceActivity;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class AsterTriggerRefreshListKeys implements IAction {

    @Override
    public JsonObject run(JsonObject args) {

        //App name is used by pubsub messaging service #getServiceName(), which is injected as a Guice module
        String APP_NAME = "scheduled-payment-event";
        //Cluster name is directly used by CipherSSOTokenProvider
        String CLUSTER_NAME = "upi";

        Injector injector = Guice.createInjector(
                new SettingsModule("config/settings.properties"),
                new AbstractModule() {
                    @Override
                    protected void configure() {

                    }

                    @Provides
                    public PubSubMessagingService pubSubMessagingService() {
                        return new PubSubMessagingService() {
                            @Override
                            public void register(CompositeNodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                            }

                            @Override
                            public void addSubscription(Subscription subscription) {

                            }

                            @Override
                            public Subscription getOutgoingSubscription(String topic) {
                                return null;
                            }

                            @Override
                            public Subscription getSubscription(String topic) {
                                return null;
                            }

                            @Override
                            public CompletionStage<Void> registerOMSSubscription(String topic, String eventName, String transformerJS, Map<String, String> config) {
                                return null;
                            }

                            @Override
                            public CompletionStage<Void> registerWebhookSubscription(String topic, String eventName, String transformerJS, String webhookURL, Map<String, String> config) {
                                return null;
                            }

                            @Override
                            public void registerPubSubCallbacks(Map<String, Map<String, PubSubMessageCallBack>> pubSubCallbacks) {

                            }

                            @Override
                            public CompletionStage<Void> publish(String topic, Message.MessagePayload pubSubMessagePayload, JID from) {
                                return null;
                            }

                            @Override
                            public CompletionStage<Void> publishEvent(PubSubEvent.Builder pubSubEventBuilder) {
                                return null;
                            }

                            @Override
                            public Set<String> getPublishingTopics() {
                                return null;
                            }

                            @Override
                            public void register() {

                            }

                            @Override
                            public void register(NodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                            }

                            @Override
                            public void addClient(OMSClient client) {

                            }

                            @Override
                            public void addRequestHandler(@Nonnull Object handler) {

                            }

                            @Override
                            public void addRequestHandlers(@Nonnull List<Object> handlers) {

                            }

                            @Override
                            public void setMessageHandler(@Nonnull MessagingService.MessageHandler handler) {

                            }

                            @Override
                            public RouterMessage prepareRouterMessage(OlympusMessage message) {
                                return null;
                            }

                            @Override
                            public void send(OlympusMessage message) {

                            }

                            @Override
                            public void addInterceptor(Interceptor interceptor) {

                            }

                            @Override
                            public void addInterceptors(List<? extends Interceptor> interceptors) {

                            }

                            @Override
                            public boolean usesClient(String serviceName) {
                                return false;
                            }

                            @Override
                            public OMSClient getClient(String serviceName) {
                                return null;
                            }

                            @Override
                            public SerialProcessor getSerialProcessor() {
                                return null;
                            }

                            @Override
                            public boolean isRegistrationAttempted() {
                                return false;
                            }

                            @Override
                            public String getServiceName() {
                                return APP_NAME;
                            }

                            @Override
                            public JID getInstanceJID() {
                                return null;
                            }

                            @Override
                            public RouterMessage createMessage() {
                                return null;
                            }

                            @Override
                            public RouterMessage copyMessage(ImmutableMessage immutableMessage) {
                                return null;
                            }

                            @Override
                            public ImmutableMessage send(RouterMessage rMessage) {
                                return null;
                            }

                            @Override
                            public void setSpartaLoadBalanced(boolean isSpartaLoadBalanced) {

                            }

                            @Override
                            public Spartan getSpartan() {
                                return null;
                            }

                            @Override
                            public boolean isLocalNode(JID jid) {
                                return false;
                            }

                            @Override
                            public void onInstanceUp(RemoteServiceInstance instance) {

                            }

                            @Override
                            public void onInstanceDown(RemoteServiceInstance instance) {

                            }

                            @Override
                            public void register(ImmutableMessage.Listener messageListener, NodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                            }

                            @Override
                            public void unRegister() {

                            }

                            @Override
                            public ImmutableMessage send(JID jid, String message) {
                                return null;
                            }

                            @Override
                            public String getListeningAddress() {
                                return null;
                            }

                            @Override
                            public boolean isRegistered() {
                                return false;
                            }

                            @Override
                            public void setInfo(String key, String value) {

                            }

                            @Override
                            public String getInfo(String key) {
                                if ("container_type".equals(key)) {
                                    return "aster";
                                }
                                return null;
                            }

                            @Override
                            public ServiceActivity getOutgoingActivityTracker() {
                                return null;
                            }

                            @Override
                            public ServiceActivity getIncomingActivityTracker() {
                                return null;
                            }

                            @Override
                            public ListenableFuture<Set<ImmutableMessage>> sendToAllInstances(String serviceType, RouterMessage routerMessage) {
                                return null;
                            }

                            @Override
                            public List<Integer> getLocalBuckets() {
                                return null;
                            }
                        };
                    }
                }
        );

        final TokenService tokenService = injector.getInstance(TokenService.class);

        long t1 = System.currentTimeMillis();
        String tenantToken = tokenService.getTenantToken(151613L).toCompletableFuture().join();
        System.out.println("Tenant token generation took " + (System.currentTimeMillis() - t1) + "ms\nToken:\n");
        System.out.println(tenantToken);
        String authToken = "Bearer " + tenantToken;

        JsonObject jsonResponse = new JsonObject();
        System.out.println("Aster Execution started");
        System.out.println("Aster args: " + args.toString());
        String keyType = args.get("keyType").getAsString();
        String timeStamp = DateUtils.currentTimeStampWithNPCIDefaultOffset();
        String txnId = IdGenerator.generateTrxnId("HBZ");
        String refId = IdGenerator.generateCustRefId();
        String orgId = args.get("orgId").getAsString();
        String prefixUrl = args.get("prefixUrl").getAsString();
        String suffixUrl = args.get("suffixUrl").getAsString();
        String refreshUrl = prefixUrl+ ":" +txnId+suffixUrl;
        try {
            String rawData =
                    "{\n" +
                    "    \"head\": {\n" +
                    "        \"ver\": \"2.0\",\n" +
                    "        \"ts\": \""+ timeStamp +"\",\n" +
                    "        \"orgId\": \""+ orgId +"\",\n" +
                    "        \"msgId\": \""+ txnId +"\",\n" +
                    "        \"pageSize\": \"1000\"\n" +
                    "    },\n" +
                    "    \"txn\": {\n" +
                    "        \"id\": \""+ txnId +"\",\n" +
                    "        \"note\": \"\",\n" +
                    "        \"refId\": \""+ refId +"\",\n" +
                    "        \"refUrl\": \"http://172.18.33.5/\",\n" +
                    "        \"ts\": \""+ timeStamp +"\",\n" +
                    "        \"type\": \""+ keyType +"\",\n" +
                    "        \"orgMsgId\": \""+ txnId +"\",\n" +
                    "        \"custRef\": \""+ refId +"\"\n" +
                    "    },\n" +
                    "    \"creds\": {\n" +
                    "        \"cred\": [\n" +
                    "            {\n" +
                    "                \"type\": \"CHALLENGE\",\n" +
                    "                \"subType\": \"INITIAL\"\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}";
            String type = "application/json";
            String encodedData = URLEncoder.encode( rawData, "UTF-8" );
            System.out.println("Aster request: " + rawData);
            URL u = new URL(refreshUrl);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty( "Content-Type", type );
            conn.setRequestProperty( "Content-Length", String.valueOf(encodedData.length()));
            conn.setRequestProperty("Authorization", authToken);
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = rawData.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }
            String res = new BufferedReader(new InputStreamReader(conn.getInputStream()))
                    .lines().parallel().collect(Collectors.joining("\n"));
            jsonResponse = new Gson().fromJson(res, JsonObject.class);
            System.out.println(res);
            conn.disconnect();
        } catch (Exception e) {
            System.out.println("Exception 1.1" + e);
        }
        return jsonResponse;
    }

//    public static void main(String[] args) {
//
//        JsonObject json = new JsonObject();
//        json.addProperty("keyType", "LIST_PSP_KEYS");
//        json.addProperty("orgId", "400033");
//        json.addProperty("prefixUrl", "https://upi.internal.mum1-stage.zetaapps.in/npci-connector/v1/urn:txnid");
//        json.addProperty("suffixUrl", "/forceRefreshListKeys");
//        AsterTriggerRefreshListKeys asterTriggerRefreshListKeys = new AsterTriggerRefreshListKeys();
//        JsonObject jsonObject = asterTriggerRefreshListKeys.run(json);
//        int a = 6;
//    }
}
