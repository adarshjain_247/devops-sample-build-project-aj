package in.zeta.upi;

import com.google.gson.JsonObject;

/**
 * Do not want to add any Unit testing libraries hence written a main method to test.
 * This is working as expected..
 */
public class ScheduledExpiredMandateEventTest {

  public static void main(String[] args) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("url","https://upi.internal.mum1-stage.zetaapps.in/upi-mandate/upi/v1/tenants/151613/mandate/expire");
    ScheduledExpiredMandateEvent scheduledExpiredMandateEvent = new ScheduledExpiredMandateEvent();
    scheduledExpiredMandateEvent.run(jsonObject);
  }
}
