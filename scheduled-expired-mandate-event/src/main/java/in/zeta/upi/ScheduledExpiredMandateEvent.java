package in.zeta.upi;

import action.IAction;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.JsonObject;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import in.zeta.commons.settings.SettingsModule;
import in.zeta.crux.services.TokenService;
import olympus.common.JID;
import olympus.message.processor.Interceptor;
import olympus.message.processor.MessagingService;
import olympus.message.processor.OMSClient;
import olympus.message.processor.serial.SerialProcessor;
import olympus.message.types.Message;
import olympus.message.types.OlympusMessage;
import olympus.pubsub.CompositeNodeDrainHandler;
import olympus.pubsub.PubSubMessagingService;
import olympus.pubsub.model.PubSubEvent;
import olympus.pubsub.subscription.PubSubMessageCallBack;
import olympus.pubsub.subscription.Subscription;
import olympus.spartan.ImmutableMessage;
import olympus.spartan.NodeDrainHandler;
import olympus.spartan.RegistrationHandler;
import olympus.spartan.RemoteServiceInstance;
import olympus.spartan.RouterMessage;
import olympus.spartan.Spartan;
import olympus.spartan.lookup.BucketDrainHandler;
import olympus.spartan.util.ServiceActivity;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class ScheduledExpiredMandateEvent implements IAction {

  @Override
  public JsonObject run(JsonObject args) {

    //App name is used by pubsub messaging service #getServiceName(), which is injected as a Guice module
    String APP_NAME = "scheduled-payment-event";
    //Cluster name is directly used by CipherSSOTokenProvider
    String CLUSTER_NAME = "upi";

    Injector injector = Guice.createInjector(
            new SettingsModule("config/settings.properties"),
            new AbstractModule() {
              @Override
              protected void configure() {

              }

              @Provides
              public PubSubMessagingService pubSubMessagingService() {
                return new PubSubMessagingService() {
                  @Override
                  public void register(CompositeNodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                  }

                  @Override
                  public void addSubscription(Subscription subscription) {

                  }

                  @Override
                  public Subscription getOutgoingSubscription(String topic) {
                    return null;
                  }

                  @Override
                  public Subscription getSubscription(String topic) {
                    return null;
                  }

                  @Override
                  public CompletionStage<Void> registerOMSSubscription(String topic, String eventName, String transformerJS, Map<String, String> config) {
                    return null;
                  }

                  @Override
                  public CompletionStage<Void> registerWebhookSubscription(String topic, String eventName, String transformerJS, String webhookURL, Map<String, String> config) {
                    return null;
                  }

                  @Override
                  public void registerPubSubCallbacks(Map<String, Map<String, PubSubMessageCallBack>> pubSubCallbacks) {

                  }

                  @Override
                  public CompletionStage<Void> publish(String topic, Message.MessagePayload pubSubMessagePayload, JID from) {
                    return null;
                  }

                  @Override
                  public CompletionStage<Void> publishEvent(PubSubEvent.Builder pubSubEventBuilder) {
                    return null;
                  }

                  @Override
                  public Set<String> getPublishingTopics() {
                    return null;
                  }

                  @Override
                  public void register() {

                  }

                  @Override
                  public void register(NodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                  }

                  @Override
                  public void addClient(OMSClient client) {

                  }

                  @Override
                  public void addRequestHandler(@Nonnull Object handler) {

                  }

                  @Override
                  public void addRequestHandlers(@Nonnull List<Object> handlers) {

                  }

                  @Override
                  public void setMessageHandler(@Nonnull MessagingService.MessageHandler handler) {

                  }

                  @Override
                  public RouterMessage prepareRouterMessage(OlympusMessage message) {
                    return null;
                  }

                  @Override
                  public void send(OlympusMessage message) {

                  }

                  @Override
                  public void addInterceptor(Interceptor interceptor) {

                  }

                  @Override
                  public void addInterceptors(List<? extends Interceptor> interceptors) {

                  }

                  @Override
                  public boolean usesClient(String serviceName) {
                    return false;
                  }

                  @Override
                  public OMSClient getClient(String serviceName) {
                    return null;
                  }

                  @Override
                  public SerialProcessor getSerialProcessor() {
                    return null;
                  }

                  @Override
                  public boolean isRegistrationAttempted() {
                    return false;
                  }

                  @Override
                  public String getServiceName() {
                    return APP_NAME;
                  }

                  @Override
                  public JID getInstanceJID() {
                    return null;
                  }

                  @Override
                  public RouterMessage createMessage() {
                    return null;
                  }

                  @Override
                  public RouterMessage copyMessage(ImmutableMessage immutableMessage) {
                    return null;
                  }

                  @Override
                  public ImmutableMessage send(RouterMessage rMessage) {
                    return null;
                  }

                  @Override
                  public void setSpartaLoadBalanced(boolean isSpartaLoadBalanced) {

                  }

                  @Override
                  public Spartan getSpartan() {
                    return null;
                  }

                  @Override
                  public boolean isLocalNode(JID jid) {
                    return false;
                  }

                  @Override
                  public void onInstanceUp(RemoteServiceInstance instance) {

                  }

                  @Override
                  public void onInstanceDown(RemoteServiceInstance instance) {

                  }

                  @Override
                  public void register(ImmutableMessage.Listener messageListener, NodeDrainHandler nodeDrainHandler, BucketDrainHandler bucketDrainHandler, RegistrationHandler registrationHandler) {

                  }

                  @Override
                  public void unRegister() {

                  }

                  @Override
                  public ImmutableMessage send(JID jid, String message) {
                    return null;
                  }

                  @Override
                  public String getListeningAddress() {
                    return null;
                  }

                  @Override
                  public boolean isRegistered() {
                    return false;
                  }

                  @Override
                  public void setInfo(String key, String value) {

                  }

                  @Override
                  public String getInfo(String key) {
                    if ("container_type".equals(key)) {
                      return "aster";
                    }
                    return null;
                  }

                  @Override
                  public ServiceActivity getOutgoingActivityTracker() {
                    return null;
                  }

                  @Override
                  public ServiceActivity getIncomingActivityTracker() {
                    return null;
                  }

                  @Override
                  public ListenableFuture<Set<ImmutableMessage>> sendToAllInstances(String serviceType, RouterMessage routerMessage) {
                    return null;
                  }

                  @Override
                  public List<Integer> getLocalBuckets() {
                    return null;
                  }
                };
              }
            }
    );


    final TokenService tokenService = injector.getInstance(TokenService.class);

    long t1 = System.currentTimeMillis();
    String tenantToken = tokenService.getTenantToken(151613L).toCompletableFuture().join();
    System.out.println("Tenant token generation took " + (System.currentTimeMillis() - t1) + "ms\nToken:\n");
    System.out.println(tenantToken);

    System.out.println("Aster job, expired mandate started..");
    System.out.println("Aster args: " + args.toString());
    try {
      String url = args.get("url").getAsString();
      String type = "application/json";
      String authToken = "Bearer " + tenantToken;
      URL u = new URL(url);
      HttpURLConnection conn = (HttpURLConnection) u.openConnection();
      //PATCH does not seem to be supported directly with java.net library.. Googled to get some help and find out this way would work
      //https://community.oracle.com/tech/developers/discussion/4159778/httpurlconnection-invalid-http-method-patch
      allowMethods("PATCH");
      conn.setDoOutput(true);
      conn.setRequestMethod("PATCH");
      conn.setRequestProperty("Content-Type", type);
      conn.setRequestProperty("Authorization", authToken);
      System.out.println(conn.getRequestMethod());
      //conn.getOutputStream().close();
      String res = new BufferedReader(new InputStreamReader(conn.getInputStream()))
        .lines().parallel().collect(Collectors.joining("\n"));
      System.out.println(res);
      conn.disconnect();
    } catch (Exception exception) {
      System.out.println(exception);
    }
    return args;
  }

  public static void allowMethods(String methods) {

    try {

      Field methodsField = HttpURLConnection.class.getDeclaredField("methods");

      Field modifiersField = Field.class.getDeclaredField("modifiers");

      modifiersField.setAccessible(true);

      modifiersField.setInt(methodsField,methodsField.getModifiers() & ~Modifier.FINAL);

      methodsField.setAccessible(true);

      String[] oldMethods = (String[]) methodsField.get(null);

      Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldMethods));

      methodsSet.addAll(Arrays.asList(methods));

      String[] newMethods = methodsSet.toArray(new String[0]);

      methodsField.set(null, newMethods);

    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new IllegalStateException(e);
    }

  }

//      public static void main(String[] args) {
//        JsonObject json = new JsonObject();
//        json.addProperty("orgId", "400033");
//        json.addProperty("url", "https://upi.internal.mum1-stage.zetaapps.in/upi-mandate/upi/v1/tenants/151613/mandate/expire");
//        ScheduledExpiredMandateEvent scheduledExpiredMandateEvent = new ScheduledExpiredMandateEvent();
//        JsonObject jsonObject = scheduledExpiredMandateEvent.run(json);
//        int a = 6;
//    }

}