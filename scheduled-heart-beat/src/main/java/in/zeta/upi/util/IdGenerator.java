package in.zeta.upi.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.security.SecureRandom;
import java.time.LocalDateTime;

import static java.lang.String.format;

public class IdGenerator {
    public static final String VALID_ID_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public static final int ID_SUFFIX_LENGTH = 32;
    public static final int START = 0;
    public static final int STAN_LENGTH = 6;
    public static final String JULIAN_DATE_FORMAT = "%03d";
    public static final String HOUR_FORMAT = "%02d";


    private IdGenerator() {
    }

    public static String generateTrxnId(String bankCode) {
        return generateId(bankCode);
    }

    private static String generateId(String prefix) {
        return prefix + RandomStringUtils.random(IdGenerator.ID_SUFFIX_LENGTH, START, VALID_ID_CHARS.length(), false,
                false, VALID_ID_CHARS.toCharArray(), new SecureRandom());
    }

    public static String generateCustRefId() {
    /*
    CustRefId is the RRN in case of UPI
    RRN is of 12 length
    RRN = lastDigitOfYear + julianDate + hour + stan
    where, julianDate is day of year
         , stan is a 6 digit random number
     */
        LocalDateTime now = LocalDateTime.now();
        int julianDate = now.getDayOfYear();
        int hour = now.getHour();
        int lastDigitOfYear = now.getYear() % 10;
        String stan = generateRandomNumber();
        return lastDigitOfYear + format(JULIAN_DATE_FORMAT, julianDate) + format(HOUR_FORMAT, hour) + stan;
    }

    private static String generateRandomNumber() {
        return RandomStringUtils.random(IdGenerator.STAN_LENGTH, START, VALID_ID_CHARS.length(), false,
                true, VALID_ID_CHARS.toCharArray(), new SecureRandom());
    }

}